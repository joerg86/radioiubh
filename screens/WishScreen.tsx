import React, { useEffect, useState } from "react";
import { Text, ImageBackground, ScrollView, TextInput, Button } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import styles, { primaryColorLight, secondaryColor } from "../lib/styles";
const WishesImage = require("../assets/wishes.jpg");
import RadioClient, { Wunsch } from "../lib/RadioClient";
import { WishList } from "../components/WishList";
import WishForm from "../components/WishForm";

/**
 * Die Hauptkomponente des Abschnitts "Musikwünsche"
 * 
 * @param props Props
 */
function WishScreen(props) {

    // State-Variable, die die Liste der eingereichten Musikwünsche verwaltet
    const [wishes, setWishes] = useState<Wunsch[]>([]);

    // Flag, das beim Auffrischen der Wunschliste genutzt wird
    let refreshing = false;

    /** Aktualisiert die Liste der Wünsche aus dem Backend des Senders */
    async function updateWishes() {
        refreshing = true;
        setWishes(await RadioClient.getWishes());
        refreshing = false;
    }

    // Stößt das periodische Update der Wünsche alle 30 Sekunden beim ersten 
    // Rendern der Komponente an
    useEffect(() => {
        updateWishes();
        const interval = setInterval(updateWishes, 30000);
        return () => clearInterval(interval);
    }, [])

    return (
        <ImageBackground source={WishesImage} style={styles.container} imageStyle={styles.backgroundImage}>
            <ScrollView contentContainerStyle={styles.view}>
                <Text style={styles.h1}>Wünsch dir einen Titel</Text>
                <WishForm onAfterSubmit={updateWishes}/>
                <WishList 
                    wishes={wishes} 
                    onRefresh={updateWishes}
                    refreshing={refreshing}
                    style={{marginTop:10}}
                />
            </ScrollView>
        </ImageBackground>
    )
}

// Konfiguration der Tab-Navigation
WishScreen.navigationOptions = {
    title: "Musikwunsch",
    tabBarIcon: ({tintColor}) => <Ionicons name="md-musical-notes" size={24} style={{color: tintColor}}/>

}

export default WishScreen;