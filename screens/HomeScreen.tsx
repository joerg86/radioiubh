import React, { useEffect, useState } from "react";
import { Text, View, StyleSheet, ActivityIndicator, Image, ImageBackground } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import RadioClient from "../lib/RadioClient";
import {LiveInfo as LiveInfoType} from "../lib/RadioClient";
import LiveInfo from "../components/LiveInfo";
import styles, { textColor, headlineColor } from "../lib/styles";
const MixerImage = require("../assets/mixer.jpg");

/**
 * Startseite der App (Abschnitt "Live")
 */
function HomeScreen(props) {
    /** State-Variable für die Live-Informationen */
    const [liveInfo, setLiveInfo] = useState<LiveInfoType>(null);
    
    /** Stößt das periodische Update der Programminformationen alle 10 Sekunden an */
    useEffect(() => {
        async function fetchData() {
            setLiveInfo(await RadioClient.getLiveInfo());
        }
        fetchData();
        const timer = setInterval(fetchData, 10000);
        return () => clearInterval(timer);
    }, [])


    return (
            <ImageBackground resizeMode="cover" source={MixerImage} style={styles.container} imageStyle={styles.backgroundImage}>
                <View style={styles.view}>
                    <Text style={styles.h1}>Radio IUBH</Text>
                    <Ionicons name="ios-radio" size={96} style={{padding: 20, color: textColor}}/>

                    { !liveInfo ? 
                            <ActivityIndicator size="large" color={textColor} style={{margin:20}}/> 
                        :
                            <LiveInfo liveInfo={liveInfo}/>
                    }

                </View>

            </ImageBackground>
    )
}

// Konfiguration der Tab-Navigation
HomeScreen.navigationOptions = {
    title: "Live",
    tabBarIcon: ({tintColor}) => <Ionicons name="md-radio" size={24} style={{color: tintColor}}/>

}

export default HomeScreen;