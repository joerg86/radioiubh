import React, { useRef } from "react";
import { Text, ImageBackground, ScrollView,View, KeyboardAvoidingView } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import styles from "../lib/styles";
const FeedbackImage = require("../assets/feedback.jpg");
import FeedbackForm from "../components/FeedbackForm";

/**
 * Abschnitt "Feedback" in der App
 */
function FeedbackScreen(props: object) {
    const scrollView = useRef();

    return (
        <KeyboardAvoidingView style={styles.container} enabled behavior="padding">
            <ImageBackground source={FeedbackImage} style={styles.container} imageStyle={styles.backgroundImage}>
                <ScrollView 
                    ref={scrollView} 
                    style={styles.container } 
                    contentContainerStyle={[styles.view, {flex:0}]} 
                    onLayout={() => (scrollView.current as ScrollView).scrollToEnd() }
                    >
                        <Text style={styles.h1}>Sag uns deine Meinung</Text>
                        <FeedbackForm/>
                </ScrollView>
            </ImageBackground>
        </KeyboardAvoidingView>

    )
}

// Konfiguration der Tab-Navigation
FeedbackScreen.navigationOptions = {
    title: "Feedback",
    tabBarIcon: ({tintColor}) => <Ionicons name="md-star" size={24} style={{color: tintColor}}/>
}

export default FeedbackScreen;