import React from 'react';

import {createAppContainer, NavigationContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import HomeScreen from "./screens/HomeScreen";
import FeedbackScreen from "./screens/FeedbackScreen";
import { createBottomTabNavigator } from "react-navigation-tabs";
import WishScreen from './screens/WishScreen';
import styles, { darkBackgroundColor, secondaryColor } from './lib/styles';


const MainNavigator = createBottomTabNavigator({
    Home: HomeScreen,
    Musikwunsch: WishScreen,
    Feedback: FeedbackScreen
  },
  {
    tabBarOptions: {
      style: { backgroundColor: darkBackgroundColor, height: 64 },
      activeTintColor: secondaryColor,
      inactiveTintColor: "white",
      labelStyle: {
        fontSize: 12, 
      }
    }
  }
);

const App: NavigationContainer = createAppContainer(MainNavigator);

export default App;