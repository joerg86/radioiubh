import React from "react";
import { Text, View } from "react-native";
import styles from "./LiveInfo.styles";
import { LiveInfo as LiveInfoType } from "../lib/RadioClient";

/**
 * Props der Live-Info-Komponente
 */
interface LiveInfoProps {
    /** LiveInfo-Objekt, das gerendert werden soll */
    liveInfo: LiveInfoType
}

/**
 * Zeigt aktuelle Informationen zum Radioprogramm (Playlist, Moderator)
 */
export default function LiveInfo({liveInfo} : LiveInfoProps) {

    return (
    <>
        <Text style={styles.defaultText}>Am Mikrofon:</Text>
        <Text style={styles.moderator}>{liveInfo.aktuellerModerator.vorname} {liveInfo.aktuellerModerator.nachname}</Text>
        <Text style={styles.defaultText}>Es läuft:</Text>

        <View style={styles.songBox}>
            <Text style={styles.songInterpret}>{liveInfo.aktuellerTitel.interpret}</Text>
            <Text style={styles.songTitle}>{liveInfo.aktuellerTitel.titel}</Text>
            { liveInfo.aktuellerTitel.album && 
                <Text style={styles.defaultText}>{liveInfo.aktuellerTitel.album}</Text>
            }
        </View>
        <Text style={styles.defaultText}>
            Davor lief:
        </Text>
        <Text style={styles.previousSong}>
            {liveInfo.vorherigerTitel.interpret + " - "}
            {liveInfo.vorherigerTitel.titel}
            {liveInfo.vorherigerTitel.album ? ` - ${liveInfo.vorherigerTitel.album}` : ""}
        </Text>
    </>
    )
}