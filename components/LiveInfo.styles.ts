import globalStyles, { textColor } from "../lib/styles";
import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    ...globalStyles,
    songTitle: {
        fontWeight: "bold",
        fontSize: 20,
        color:textColor,
        textAlign: "center"
    },
    songInterpret: {
        color: textColor,
        fontSize: 16,
        textAlign: "center"
    },
    moderator: {
        fontWeight: "bold",
        fontSize: 20,
        padding: 10,
        color: textColor,
    },
    songBox: {
        alignItems: "center",
        justifyContent: "center",
        borderColor: textColor,
        borderWidth: StyleSheet.hairlineWidth,
        padding: 20,
        margin: 10,
        minHeight: 100,
        width: "80%",
        borderRadius: 5,
        backgroundColor: "#0001"
    },
    previousSong: {
        ...globalStyles.defaultText,
        textAlign: "center"
    }
})

export default styles;