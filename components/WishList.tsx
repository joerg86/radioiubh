import React from "react";
import { FlatList, Text, StyleSheet, View } from "react-native";
import { Wunsch } from "../lib/RadioClient";
import globalStyles from "../lib/styles";
import moment from "moment";
import "moment/locale/de";
moment.locale("de");

const styles = StyleSheet.create({
    ...globalStyles,

    wishListItem: {
        padding: 10,
        margin: 0,
        borderBottomWidth: StyleSheet.hairlineWidth,
        
    }
})

interface WishListProps {
    wishes: Wunsch[];
    [propName: string]: any;
}

interface WishListItemProps {
    item: Wunsch;
    [propName: string]: any;
}

function WishListItem({item} : WishListItemProps) {
    return (
        <View style={styles.wishListItem}>
            <Text style={[styles.h2]}>{item.interpret} - {item.titel}</Text>
            <Text style={[styles.defaultText, {fontStyle: "italic"}]}>{moment(item.timestamp).fromNow()} gewünscht von {item.autor}</Text>
        </View>
    )
}

export function WishList({wishes, ...props} : WishListProps) {
    return (
        <FlatList 
            inverted={false}
            data={wishes} 
            renderItem={({item}) => <WishListItem item={item}/>}
            keyExtractor={(item) => item.id.toString()}
            {...props}
        />
    )
}