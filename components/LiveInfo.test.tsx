import renderer from 'react-test-renderer';
import LiveInfo from './LiveInfo';
import React from "react";

test("renders correctly", () => {
    const testInfo = {
        aktuellerModerator: {
            vorname: "Theresa",
            nachname: "Tester"
        },
        aktuellerTitel: {
            titel: "Testtitel",
            interpret: "Michael Jackson",
            album: "Foobar"
        },
        vorherigerTitel: {
            titel: "123 Polizei",
            interpret: "Unbekannt"
        }

    }
    const tree = renderer.create(<LiveInfo liveInfo={testInfo}/>).toJSON();
    expect(tree).toMatchSnapshot();
})