import React from "react";
import { View, Button, Alert, TextInput, Text, StyleSheet } from "react-native";
import globalStyles, { secondaryColor, textColor, primaryColorLight } from "../lib/styles";
import { Formik, yupToFormErrors } from 'formik';
import * as Yup from "yup";
import RadioClient, { Wunsch } from "../lib/RadioClient";
import StarRating from "react-native-star-rating";

const requiredMsg = "Pflichtfeld"

// Validiert die Formular-Eingaben zum Musikwunsch
const validationSchema = Yup.object().shape({
    autor: Yup.string(),
    kommentar: Yup.string(),
    sterneModerator: Yup.number().min(1, "Bitte den Moderator bewerten").required("blub"),
    sternePlaylist: Yup.number().min(1,"Bitte die Playlist bewerten").required("bla")
})

/**
 * Props des Bewertungsformulars
 */
interface FeedbackFormProps{
    /** Callback, das nach dem Absenden der Bewertung aufgerufen wird */
    onSubmit?: () => any;
    [propName: string]: any;
}

/** Spezifische Stylesheets für das Bewertungsformular */
const styles = StyleSheet.create({
    ...globalStyles,
    stars: {
        margin: 10
    },
    star: {
        margin: 5,

    },
    rating: {
        marginTop: 10,
        marginBottom: 10,
        alignItems: "center"
    }
})

/**
 * Formular, das eine Bewertung entgegennimmt und an das Backend sendet
 */
export default function FeedbackForm(props: FeedbackFormProps) {

    async function sendWish(values) {
        await RadioClient.sendFeedback({...values});
        Alert.alert("Vielen Dank!", "Dein Feedback wurde erfolgreich gesendet.")
        if(props.onSubmit) {
            props.onSubmit();
        }
    }
    return (
    <Formik initialValues={{sterneModerator: 0, sternePlaylist:0 }} onSubmit={sendWish} validationSchema={validationSchema}>
        {({handleChange, handleBlur, handleSubmit, isValid, values, errors, touched, setFieldValue, submitCount}) =>
            <View style={styles.form}>
                <View style={styles.rating}>
                    <Text style={styles.h2}>Wie gefällt dir der Moderator?</Text>
                    <StarRating starStyle={styles.star} containerStyle={styles.stars} fullStarColor={primaryColorLight} 
                        maxStars={5} rating={values.sterneModerator} selectedStar={(rating) => setFieldValue("sterneModerator", rating)}/>
                        {(touched.sterneModerator && errors.sterneModerator) && <Text style={styles.feedback}>{errors.sterneModerator}</Text>}
                </View>

                <View style={styles.rating}>
                    <Text style={styles.h2}>Wie gefällt dir die Playlist?</Text>
                    <StarRating starStyle={styles.star} containerStyle={styles.stars} fullStarColor={primaryColorLight} 
                        maxStars={5} rating={values.sternePlaylist} selectedStar={(rating) => setFieldValue("sternePlaylist", rating)}/>
                        {(touched.sternePlaylist && errors.sternePlaylist) && <Text style={styles.feedback}>{errors.sternePlaylist}</Text>}

                </View>


                <TextInput placeholder="Kommentar (optional)" style={styles.input} maxLength={200} multiline numberOfLines={5} onBlur={handleBlur("kommentar")} onChangeText={handleChange("kommentar")}/>

                <TextInput placeholder="Dein Name (optional)" style={styles.input} maxLength={50} onBlur={handleBlur("autor")} onChangeText={handleChange("autor")}/>

                <Button title="Dein Feedback senden" color={secondaryColor} onPress={handleSubmit as any}/>
                
            </View>
        }   
    </Formik>
    )
}