import React from "react";
import { View, Button, Alert, TextInput, Text, StyleSheet, Keyboard } from "react-native";
import styles, { secondaryColor } from "../lib/styles";
import { Formik } from 'formik';
import * as Yup from "yup";
import RadioClient, { Wunsch } from "../lib/RadioClient";

const requiredMsg = "Pflichtfeld"

// Validiert die Formular-Eingaben zum Musikwunsch
const validationSchema = Yup.object().shape({
    autor: Yup.string().required(requiredMsg),
    titel: Yup.string().required(requiredMsg),
    interpret: Yup.string().required(requiredMsg)
})

/** 
 * Props des Wunschformulars
 */
interface WishFormProps{
    /** Callback, das nach dem Absenden des Wunsches aufgerufen wird */
    onAfterSubmit?: () => any;
    [propName: string]: any;
}

/**
 * Formular zur Erstellung eines Musikwunsches
 */
export default function WishForm(props: WishFormProps) {

    /** Absenden des Wunsches */
    async function sendWish(values, actions) {
        Keyboard.dismiss();
        await RadioClient.sendWish({...values});
        actions.resetForm({});
        
        Alert.alert("Gesendet", "Dein Wunsch wurde erfolgreich gesendet.")
        if(props.onAfterSubmit) {
            props.onAfterSubmit();
        }
    }

    return (
    <Formik 
        initialValues={{titel: "", interpret: "", autor: ""}} 
        onSubmit={sendWish} 
        validationSchema={validationSchema}>

        {({handleChange, handleBlur, handleSubmit, isValid, errors, touched, values}) =>
            <View style={styles.form}>

                <TextInput value={values.titel} placeholder="Dein Wunschtitel" style={styles.input} onBlur={handleBlur("titel")} onChangeText={handleChange("titel")}/>
                {(touched.titel && errors.titel) && <Text style={styles.feedback}>{errors.titel}</Text>}

                <TextInput value={values.interpret} placeholder="Interpret" style={styles.input} onBlur={handleBlur("interpret")} onChangeText={handleChange("interpret")}/>
                {(touched.interpret && errors.interpret) && <Text style={styles.feedback}>{errors.interpret}</Text>}

                <TextInput value={values.autor} placeholder="Dein Name" style={styles.input} onBlur={handleBlur("autor")} onChangeText={handleChange("autor")}/>
                {(touched.autor && errors.autor) && <Text style={styles.feedback}>{errors.autor}</Text>}

                <Button title="Deinen Wunsch senden" color={secondaryColor} onPress={handleSubmit as any}/>
                
            </View>
        }   

    </Formik>
    )
}