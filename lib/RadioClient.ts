
/** Repräsentiert einen Musiktitel */
export  interface Titel {
    titel: string;
    interpret: string;
    album?: string;
}

/** Repräsentiert einen Radiomoderator */
export interface Moderator {
    vorname: string;
    nachname: string;
}

/** Repräsentiert Live-Informationen zum Radioprogramm */
export interface LiveInfo {
    aktuellerTitel: Titel;
    vorherigerTitel: Titel;
    aktuellerModerator: Moderator;
}

/** Repräsentiert eine Bewertung */
export interface Feedback {
    sterneModerator: number;
    sternePlaylist: number;
    kommentar: string;
    autor: string;
    timestamp?: Date;
}

/** Repräsentiert einen Musikwunsch */
export interface Wunsch {
    id: number;
    titel: string;
    interpret: string;
    autor: string;
    timestamp: Date;
}

const DEMO_TITEL : Titel[] = [
    {
        titel: "Bestes Leben",
        interpret: "Silber",
        album: "Schritte"
    },
    {
        titel: "Dance Monkey",
        interpret: "Tones and I"
    },
    {
        titel: "An guten Tagen",
        interpret: "Johannes Oerding",
        album: "Konturen"
    },
    {
        titel: "Skifahren (feat. Joshi Mizu)",
        interpret: "The Cratez, Bausa & Maxwell"
    },
    {
        titel: "Happy Ending",
        interpret: "Mika"
    }
]

const DEMO_MODERATOR = {
    vorname: "Stefan",
    nachname: "Sommerfeld"
}

const demoLiveInfo : LiveInfo = {
    aktuellerModerator: DEMO_MODERATOR,
    aktuellerTitel: DEMO_TITEL[1],
    vorherigerTitel: DEMO_TITEL[2]
}

const MS_PER_MINUTE = 60000;

/**
 * Subtrahiert die übergebene Anzahl Minuten von der aktuellen Zeit.
 * Wird für die Erstellung der Demodaten für die Musikwünsche benötigt.
 * 
 * @param minutes Anzahl der zu subtrahierenden Minuten
 * @returns {Date} Date-Objekt
 */
function minutesAgo(minutes: number) {
    return new Date(Date.now() - minutes * MS_PER_MINUTE)
}


// Demodaten: Musikwünsche
const demoWuensche : Wunsch[] = [
    {
        id: 1,
        autor: "Jörg",
        titel: "Let it be",
        interpret: "Beatles",
        timestamp: minutesAgo(67)
    },
    {
        id: 2,
        autor: "Veronika",
        titel: "Don't leave me this way",
        interpret: "The Communards",
        timestamp: minutesAgo(33)
    },
    {
        id: 3,
        autor: "Chantal",
        titel: "Mensch",
        interpret: "Herbert Grönemeyer",
        timestamp: minutesAgo(17)
    },
    {
        id:4,
        autor: "Bärbel",
        titel: "Dancing Queen",
        interpret: "ABBA",
        timestamp: minutesAgo(11)
    },
    {
        id:5,
        autor: "Norbert",
        titel: "Wieso tust du dir das an?",
        interpret: "Apache 207",
        timestamp: new Date()
    }

]


/**
 * Klasse mit statischen Methoden zur Kommunikation mit dem Backend-System des Senders
 * 
 * @todo Dies ist ein Dummy mit Demodaten. Die eigentliche Anbindung muss noch implementiert werden. 
 * @public
 */
export default class RadioClient {
    /**
     * Liefert Live-Informationen zum Moderator und zur Playlist.
     * 
     * @returns {Promise<LiveInfo>} Promise mit Live-Informationen
     * @public
     */
    public static async getLiveInfo() : Promise<LiveInfo> {
        return {...demoLiveInfo};
    }

    /**
     * Übergibt eine Bewertung an das Backend-System des Senders, das den Moderator 
     * im Studio sofort benachrichtigt.
     * 
     * @param {Feedback} feedback Bewertung, die gesendet werden soll
     * @public
     */
    public static async sendFeedback(feedback: Feedback) : Promise<Feedback> {
        // Hier Anbindung zum Backend implementieren
        return feedback;
    }

    /**
     * Liefert eine Liste der letzten Musikwünsche.
     * 
     * @returns {Promise<Wunsch[]>} Promise mit einer Liste von Musikwünschen
     * @public
     */
    public static async getWishes(): Promise<Wunsch[]> {
        return [...demoWuensche].reverse();
    }

    /**
     * Sendet einen Musikwunsch an das Backend-System des Senders.
     * 
     * @param wish Musikwunsch
     * @public
     */
    static async sendWish(wish: Wunsch): Promise<Wunsch> {
        wish = {
            ...wish,
            id: demoWuensche.length + 1,
            timestamp: new Date()
        };
        demoWuensche.push(wish);
        return wish;
    }
}

/**
 * Verändert zu Demozwecken die Playlist.
 */
function shufflePlaylist() {
    demoLiveInfo.vorherigerTitel = demoLiveInfo.aktuellerTitel;

    // Zufälligen Song auswählen
    demoLiveInfo.aktuellerTitel = DEMO_TITEL[Math.floor(Math.random() * DEMO_TITEL.length)]
}

// Demo-Playliste alle 10 Sekunden aktualisieren
setInterval(shufflePlaylist, 10000);

