import { StyleSheet, StatusBar } from "react-native"

export const primaryColor = "#123b48";
export const primaryColorLight = "#008f8f"
export const secondaryColor = "#ea5b0c"
export const textColor = primaryColor;
export const headlineColor = secondaryColor;
export const darkBackgroundColor = primaryColor;

/** Globale Stylsheets */
const globalStyles = StyleSheet.create({
    container: {
        display: "flex",
        flex: 1,
    },
    view: {
        marginTop: StatusBar.currentHeight,
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        color: textColor,
        fontSize: 14,
        padding: 10,
        textAlign: "center"
    },
    h1: {
        fontSize: 24,
        color: primaryColorLight,
        marginBottom: 10,
        marginTop: 10,
    },
    h2: {
        fontSize: 20,
        color: secondaryColor
    },
    defaultText: {
        color: textColor,
        fontSize: 14,
    },
    backgroundImage: {
        opacity: 0.17
    },
    tabIcon: {
        //color: "white"
    },
    input: {
        color: textColor,
        fontSize: 16,
        borderRadius:3,
        borderColor:textColor,
        padding:10,
        paddingBottom: 5,
        marginBottom: 5,
        marginTop: 5,
        width: "100%",
        minWidth: "100%",
        maxWidth: "100%",
        backgroundColor: "#fff9",
        textAlignVertical: "top"
    },
    feedback: {
        color: "red",
        fontWeight: "bold",
        marginLeft: 10,
        marginBottom: 10
    },
    form: {
        padding: 10,
    }
})
export default globalStyles;